const express = require('express');
const router = express.Router();
const usersController = require('../controllers/result/resultsController.js')

/* asumir path base /users */
router.get('/',usersController.list);

router.get('/:n1/:n2',usersController.sum);

router.post('/',usersController.mult);

router.put('/',usersController.div);

router.delete('/:n1/:n2',usersController.sub);

module.exports = router;
