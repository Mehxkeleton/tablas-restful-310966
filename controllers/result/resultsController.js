const express = require('express');

function list(req, res, next){
  let users = [];
  res.json(users);
}

function sum(req, res, next){
  let n1 = req.params.n1;
  let n2 = req.params.n2;
  let sum = parseFloat(n1)+parseFloat(n2);
  res.json(sum);
}

function mult(req, res, next){
  let n1 = req.body.n1;
  let n2 = req.body.n2;
  let mult = parseFloat(n1)*parseFloat(n2);
  res.json(mult);
}

function div(req, res, next){
  let n1 = req.body.n1;
  let n2 = req.body.n2;
  let div = parseFloat(n1)/parseFloat(n2);
  res.json(div);
}

function sub(req, res, next){
  let n1 = req.params.n1;
  let n2 = req.params.n2;
  let sub = parseFloat(n1)-parseFloat(n2);
  res.json(sub);
}

module.exports = {
  list, sum, mult, div, sub
}
